# iperf3

#### 介绍
参看本目录下的README.md文件。

#### 下载安装

直接在OpenHarmony-SIG仓（https://gitee.com/organizations/openharmony-sig/projects）中搜索iperf并下载。

#### 编译运行

iperf3目前已经可以在OpenHarmony的标准系统和小型系统（Liteos-a）上运行。 笔者以下面的环境为例，介绍如何将iperf3代码集成到OpenHarmony中。

```
OpenHarmony版本:  OpenHarmony v3.1.1 Release (2022-05-31)

      使用的设备：  Hispark_AI_Hi3516D_One_Light_VER.B
```

步骤如下：

1. 将下载的代码存入以下路径： ./developtools/iperf

2. 修改如下构建脚本：

```
    路径： ./build/lite/components/communication.json, 添加component:
    {
        "component": "iperf3",
        "description": "iPerf3 is a tool for active measurements of the maximum achievable bandwidth on IP networks.",
        "optional": "false",
        "dirs": [
            "//developtools/iperf"
        ],
        "targets": [
          "//developtools/iperf:iperf3"
        ],
        "rom": "",
        "ram": "",
        "output": [],
        "adapted_kernel": [
          "liteos_a"
        ],
        "features": [],
        "deps": {
          "components": [
          ]
        }
	}
    
    路径： ./vendor/hisilicon/hispark_taurus/config.json, 在components中添加component:
    {
        "subsystem": "communication",
	    "components": [
	   	   .......	
	       { "component": "iperf3", "features":[] }
	         
	       ]
    },
	
    路径： ./productdefine/common/products/Hi3516DV300.json, 在parts中添加iperf3:
    {
		"product_name": "Hi3516DV300",
		"product_company": "hisilicon",
		"product_device": "hi3516dv300",
		"version": "2.0",
		"type": "standard",
		"product_build_path": "device/board/hisilicon/hispark_taurus/linux",
		"parts":{
			.......
			"developtools:iperf3":{},
			....... 
		}
    }
```



3. 编译

   1) 标准系统

      ```
      ./build.sh --product-name Hi3516DV300
      ```

      

   2) 小型系统    

   ```
   1) hb set
      选择 ipcamera_hispark_taurus
   2) hb build 
   ```

     

4. 运行

   上述方式生成的镜像烧入设备后，在/bin目录下会多出一个iperf3可执行文件，本目录下的README.md文件中的**Invoking iperf3**这一节介绍使用方法。

​		



#### 常见问题

1.   执行命令后，出错如下：

```
# ./iperf3 -c 192.168.1.1

Connecting to host 192.168.1.1, port 5201
iperf3: error - unable to create a new stream: Read-only file system
```

这是因为iperf3在使用的时候需要在一个有读写权限的目录下创建一个文件用做缓存。 参见iperf_api.c中的iperf_new_stream函数。这个目录通过读系统环境变量获取， 先读取TEMPDIR环境变量，如果没有读到值，再读TEMP环境变量, 如果还没有读到值，再去读TMP环境变量，如果还没有读到值，系统使用/tmp作为默认的目录，如果/tmp目录不存在或者无法创建，就会导致上述的错误。 



 如果运行的是openHarmony标准系统，设置TMPDIR为一个可读写的目录路径能够解决这个问题，比如Hispark_AI_Hi3516D_One_Light_VER.B中/data/目录是一个可读写的目录，在命令行中设置环境变量如下：

```
export TMPDIR=/data
```

如果运行的是openHarmony小型系统，Liteos-a不支持环境变量， 可以在系统的根目录下添加tmp目录解决这个问题。如果系统没有权限添加tmp目录，可以修改iperf_api.c，如下所示：



```
struct iperf_stream *
iperf_new_stream(struct iperf_test *test, int s, int sender)
{
    struct iperf_stream *sp;
    int ret = 0;

char template[1024];
if (test->tmp_template) {
    snprintf(template, sizeof(template) / sizeof(char), "%s", test->tmp_template);
} else {
    //find the system temporary dir *unix, windows, cygwin support
    char* tempdir = getenv("TMPDIR");
    if (tempdir == 0){
        tempdir = getenv("TEMP");
    }
    if (tempdir == 0){
        tempdir = getenv("TMP");
    }
    if (tempdir == 0){
#if defined(kernel_liteos_a) && defined(ipcamera_hispark_taurus)
________________________________________________________________
            tempdir = "/userdata";
			______________________
#else
_____
            tempdir = "/tmp";
#endif
______
        }
        snprintf(template, sizeof(template) / sizeof(char), "%s/iperf3.XXXXXX", tempdir);
    }


```







